BINARY_NAME=cli-util

.PHONY: all
all: win linux

.PHONY: linux
linux:
	GOOS=linux GOARCH=amd64 go build $(RACE) -o ./bin/$(BINARY_NAME)-linux ./main.go

.PHONY: win
win:
	GOOS=windows GOARCH=amd64 go build $(RACE) -o ./bin/$(BINARY_NAME)-win.exe ./main.go

install:
	go mod tidy

build: win linux