/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import (
	"nodemessage.com/cli-util/cmd"
)

func main() {
	cmd.Execute()
}
