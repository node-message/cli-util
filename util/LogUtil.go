package util

import (
	"github.com/fatih/color"
)

func Error(msg string) {
	err := color.New(color.FgRed)
	_, _ = err.Printf(" [ error ] %s", msg)
}
