/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"errors"
	"github.com/boombuler/barcode"
	qr2 "github.com/boombuler/barcode/qr"
	"github.com/skip2/go-qrcode"
	"github.com/spf13/cobra"
	"image/color"
	"image/png"
	"nodemessage.com/cli-util/util"
	"os"
)

type qrCodeEntity struct {
	content  string
	output   string
	fileName string
}

func newQrCode(content string, output string, fileName string) *qrCodeEntity {
	return &qrCodeEntity{content: content, output: output, fileName: fileName}
}

func (q *qrCodeEntity) generate() error {
	if q.content == "" {
		return errors.New("请输入内容 ? cli-util qrcode -h")
	}
	qr, err := qrcode.New(q.content, qrcode.Medium)

	if err != nil {
		return err
	} else {
		qr.ForegroundColor = color.Black
		_ = qr.WriteFile(256, q.output+q.fileName+".png")
	}
	return nil
}

func (q *qrCodeEntity) generatePlanB() error {
	if q.content == "" {
		return errors.New("请输入内容 ? cli-util qrcode -h")
	}
	qr, err := qr2.Encode(q.content, qr2.M, qr2.Auto)
	if err != nil {
		return err
	} else {
		qr, _ = barcode.Scale(qr, 200, 200)
		file, _ := os.Create(q.output + q.fileName + ".png")
		defer file.Close()
		err := png.Encode(file, qr)
		if err != nil {
			return err
		}
	}
	return nil
}

var qrcodeCmd = &cobra.Command{
	Use:   "qrcode",
	Short: "生成二维码",
	Long:  `此工具可生成二维码图片`,
	Run: func(cmd *cobra.Command, args []string) {
		content, _ := cmd.Flags().GetString("content")
		output, _ := cmd.Flags().GetString("output")
		filename, _ := cmd.Flags().GetString("filename")
		code := newQrCode(content, output, filename)
		err := code.generatePlanB()
		if err != nil {
			util.Error(err.Error())
			return
		}
	},
}

func init() {
	rootCmd.AddCommand(qrcodeCmd)
	qrcodeCmd.Flags().StringP("content", "c", "", "二维码内容")
	qrcodeCmd.Flags().StringP("output", "o", "./", "输出位置")
	qrcodeCmd.Flags().StringP("filename", "n", "qrcode", "输出文件名")
}
